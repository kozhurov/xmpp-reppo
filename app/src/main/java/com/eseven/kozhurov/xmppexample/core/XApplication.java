package com.eseven.kozhurov.xmppexample.core;

import android.app.Application;

import com.eseven.kozhurov.xmppexample.net.XmppManager;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class XApplication extends Application {

    private Executor mExecutor;
    private XmppManager mXmppManager;
    private SharedHelper mSharedHelper;


    @Override
    public void onCreate() {
        super.onCreate();

        mExecutor = Executors.newFixedThreadPool(2);

        mSharedHelper = new SharedHelper(this);
        mXmppManager = new XmppManager(mExecutor);

    }


    public XmppManager getXmppManager() {
        return mXmppManager;
    }

    public SharedHelper getSharedHelper() {
        return mSharedHelper;
    }
}
