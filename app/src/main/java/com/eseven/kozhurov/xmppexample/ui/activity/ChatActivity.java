package com.eseven.kozhurov.xmppexample.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.eseven.kozhurov.xmppexample.R;
import com.eseven.kozhurov.xmppexample.ui.adapter.ChatAdapter;

import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.w3c.dom.Text;

import java.util.ArrayList;

public final class ChatActivity extends GenericActivity {

    private static final String CHAT_THREAD_ID = "chat_thread_id";
    private static final String OPPONENT_JID = "opponent_jid";

    private ChatAdapter mMessageListAdapter;
    private EditText mSendEt;
    private ListView mChatLv;

    private Chat mChat;
    private ChatMessageListener mMessageListener;

    public static void start(Context pContext, String pThreadId, String pUserJid) {
        Bundle bundle = new Bundle();
        bundle.putString(CHAT_THREAD_ID, pThreadId);
        bundle.putString(OPPONENT_JID, pUserJid);

        Intent intent = new Intent(pContext, ChatActivity.class);
        intent.putExtras(bundle);

        pContext.startActivity(intent);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        enableBackButton();

        mSendEt = (EditText) findViewById(R.id.ca_chat_send_input_field);
        findViewById(R.id.ca_chat_send_button).setOnClickListener(new Clicker());

        Bundle bundle = getIntent().getExtras();
        String threadId = bundle.getString(CHAT_THREAD_ID);
        String userJid = bundle.getString(OPPONENT_JID);

        mMessageListener = new MessageInterceptor();

        if (TextUtils.isEmpty(userJid)) {
            mChat = mXmppManager.connectToTheChat(threadId, mMessageListener);
        } else {
            mChat = mXmppManager.createChat(userJid, mMessageListener);
        }

        mMessageListAdapter = new ChatAdapter(this, new ArrayList<Message>(), mSharedHelper.getJid());
        mChatLv = (ListView) findViewById(R.id.ca_chat_lv);
        mChatLv.setAdapter(mMessageListAdapter);
    }

    private void sendMessage() {
        String messageBody = mSendEt.getText().toString();
        mSendEt.setText("");
        mXmppManager.sendMessageToChat(mChat, messageBody);

        Message message = new Message();
        message.setFrom(mSharedHelper.getJid());
        message.setBody(messageBody);

        mMessageListAdapter.addNewEntity(message);
        mMessageListAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mXmppManager.disconnectFromTheChat(mChat, mMessageListener);
    }

    private final class Clicker implements View.OnClickListener {

        @Override
        public void onClick(View pView) {

            switch (pView.getId()) {

                case R.id.ca_chat_send_button:
                    sendMessage();
                    break;

                case android.R.id.home:
                    finish();
                    break;

            }
        }
    }

    private final class MessageInterceptor implements ChatMessageListener {

        @Override
        public void processMessage(Chat chat, final Message message) {
            Log.e("CA", "income message = " + message.getBody());
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mMessageListAdapter.addNewEntity(message);
                }
            });
        }
    }
}
