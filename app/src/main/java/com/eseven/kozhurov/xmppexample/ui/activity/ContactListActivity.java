package com.eseven.kozhurov.xmppexample.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.eseven.kozhurov.xmppexample.R;
import com.eseven.kozhurov.xmppexample.ui.adapter.ContactAdapter;

import org.jivesoftware.smack.roster.RosterEntry;

import java.util.Collection;

public final class ContactListActivity extends GenericActivity {

    private ListView mContactListView;

    public static void start(Context pContext) {
        pContext.startActivity(new Intent(pContext, ContactListActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        initContactList();
    }

    private void initContactList() {
        mContactListView = (ListView) findViewById(R.id.co_chat_lv);
        mContactListView.setOnItemClickListener(new ItemClicker());


        Collection<RosterEntry> rosterEntryrs = mXmppManager.getRosterEntries();
        ContactAdapter adapter = new ContactAdapter(this, rosterEntryrs);

        mContactListView.setAdapter(adapter);
        mContactListView.setEmptyView(findViewById(R.id.co_empty_list));

    }


    private final class ItemClicker implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> pAdapterView, View pView, int i, long l) {
            RosterEntry entry = ((RosterEntry) pAdapterView.getItemAtPosition(i));
            ChatActivity.start(ContactListActivity.this, null, entry.getUser());
        }
    }

}
