package com.eseven.kozhurov.xmppexample.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.eseven.kozhurov.xmppexample.R;

import org.jivesoftware.smack.chat.Chat;

import java.util.List;

public final class ChatsListAdapter extends ArrayAdapter<Chat> {

    private final LayoutInflater mLayoutInflater;

    public ChatsListAdapter(Context context, List<Chat> objects) {
        super(context, 0, objects);
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

    Holder holder;

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.adapter_chat_list, parent, false);
            holder = new Holder();

            holder.mChatId = (TextView) convertView.findViewById(R.id.chat_id);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.mChatId.setText(getItem(position).getThreadID());

        return convertView;
    }


    private static final class Holder {
        TextView mChatId;
    }
}
