package com.eseven.kozhurov.xmppexample.core;

public interface MainCallBack {

    public void onSuccess();
    public void onError(String pError);
}
