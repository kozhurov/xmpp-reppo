package com.eseven.kozhurov.xmppexample.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.eseven.kozhurov.xmppexample.R;

import org.jivesoftware.smack.roster.RosterEntry;

import java.util.ArrayList;
import java.util.Collection;

public final class ContactAdapter extends ArrayAdapter<RosterEntry> {

    private final LayoutInflater mLayoutInflater;

    public ContactAdapter(Context context, Collection<RosterEntry> objects) {
        super(context, 0, new ArrayList<RosterEntry>(objects));
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

    Holder holder;

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.adapter_contacts, parent, false);
            holder = new Holder();

            holder.mContactJid = (TextView) convertView.findViewById(R.id.cad_jid);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.mContactJid.setText(getItem(position).getUser());

        return convertView;
    }


    private static final class Holder {
        TextView mContactJid;
    }
}
