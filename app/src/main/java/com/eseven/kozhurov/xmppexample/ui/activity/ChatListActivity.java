package com.eseven.kozhurov.xmppexample.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.eseven.kozhurov.xmppexample.R;
import com.eseven.kozhurov.xmppexample.ui.adapter.ChatsListAdapter;

import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManagerListener;

import java.util.ArrayList;

public final class ChatListActivity extends GenericActivity {

    private ListView mChatsListView;
    private ChatsListAdapter mChatsListAdapter;

    public static void start(Context pContext) {
        pContext.startActivity(new Intent(pContext, ChatListActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chats);

        initChatList();
    }

    private void initChatList() {
        mChatsListView = (ListView) findViewById(R.id.ch_chat_lv);
        mChatsListView.setOnItemClickListener(new ItemClicker());

        mChatsListAdapter = new ChatsListAdapter(this, new ArrayList<Chat>());

        mChatsListView.setAdapter(mChatsListAdapter);
        mChatsListView.setEmptyView(findViewById(R.id.ch_empty_list));

        mXmppManager.setChatsListener(new ChatListener());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_contacts, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_contacts:
                ContactListActivity.start(this);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private final class ChatListener implements ChatManagerListener {

        @Override
        public void chatCreated(final Chat chat, boolean createdLocally) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mChatsListAdapter.add(chat);
                }
            });

        }
    }

    private final class ItemClicker implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> pAdapterView, View pView, int i, long l) {
            Chat entry = ((Chat) pAdapterView.getItemAtPosition(i));
            ChatActivity.start(ChatListActivity.this, entry.getThreadID(), null);
        }
    }

}
