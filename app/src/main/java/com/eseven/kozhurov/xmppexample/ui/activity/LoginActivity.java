package com.eseven.kozhurov.xmppexample.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.eseven.kozhurov.xmppexample.R;
import com.eseven.kozhurov.xmppexample.core.MainCallBack;

public final class LoginActivity extends GenericActivity {

    private EditText mLoginEt;
    private EditText mPasswordEt;
    private EditText mServerEt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        View.OnClickListener listener = new Clicker();
        findViewById(R.id.ma_connect_button).setOnClickListener(listener);

        mLoginEt = (EditText) findViewById(R.id.ma_login);
        mPasswordEt = (EditText) findViewById(R.id.ma_password);
        mServerEt = (EditText) findViewById(R.id.ma_server);
    }

    private final class CallbackListener implements MainCallBack {

        @Override
        public void onSuccess() {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    hideProgressDialog();
                    showShortToast(R.string.success_toast);
                    ChatListActivity.start(LoginActivity.this);
                    finish();
                }
            });
        }

        @Override
        public void onError(final String pError) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    hideProgressDialog();
                    showShortToast(pError);
                }
            });
        }
    }

    private final class Clicker implements View.OnClickListener {

        @Override
        public void onClick(View pView) {

            switch (pView.getId()) {
                case R.id.ma_connect_button:

                    String login = mLoginEt.getText().toString();
                    String password = mPasswordEt.getText().toString();
                    String server = mServerEt.getText().toString();

                    if (TextUtils.isEmpty(login) || TextUtils.isEmpty(password) || TextUtils.isEmpty(server)) {
                        showShortToast(getString(R.string.login_error));
                        return;
                    }

                    showProgressDialog();

                    mSharedHelper.setLogin(login);
                    mSharedHelper.setJid(login + '@' + server);

                    mXmppManager.connectAndLoginToServer(new CallbackListener(), login, password);
                    break;
            }
        }
    }
}
