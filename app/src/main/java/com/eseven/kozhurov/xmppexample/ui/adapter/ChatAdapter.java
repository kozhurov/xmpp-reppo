package com.eseven.kozhurov.xmppexample.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.eseven.kozhurov.xmppexample.R;

import org.jivesoftware.smack.packet.Message;

import java.util.List;

public class ChatAdapter extends BaseAdapter {

    private static final int OUTCOME = 0;
    private static final int INCOME = 1;

    private final LayoutInflater mInflater;

    private List<Message> mMessageList;
    private String mMyJid;

    public ChatAdapter(Context context, List<Message> objects, String pMyJid) {
        mInflater = LayoutInflater.from(context);
        mMessageList = objects;
        mMyJid = pMyJid;
    }


    @Override
    public int getCount() {
        return mMessageList.size();
    }

    @Override
    public Message getItem(int i) {
        return mMessageList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return mMessageList.get(i).hashCode();
    }

    public void addNewEntity(Message pMessage){
        mMessageList.add(pMessage);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
       return mMessageList.get(position).getFrom().equals(mMyJid) ? OUTCOME : INCOME;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        Message message = getItem(position);

        if (convertView == null) {

            Log.e("Chat", "from = " + message.getFrom() + " , my id " + mMyJid +" , body = "+message.getBody());

            switch (getItemViewType(position)){
                case INCOME:
                    convertView = mInflater.inflate(R.layout.item_incoming_gs_message, parent, false);
                    break;

                case OUTCOME:
                    convertView = mInflater.inflate(R.layout.item_outgoing_gs_message, parent, false);
                    break;
            }

            holder = new ViewHolder();
            holder.mAuthor = (TextView) convertView.findViewById(R.id.im_message_author);
            holder.mBody = (TextView) convertView.findViewById(R.id.im_message);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mAuthor.setText(message.getFrom());
        holder.mBody.setText(message.getBody());

        return convertView;

    }


    private static class ViewHolder {
        TextView mAuthor;
        TextView mBody;
    }
}
