package com.eseven.kozhurov.xmppexample.core;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedHelper {

    private static final String NAME = "xmpp_example";
    private static final String LOGIN = "login";
    private static final String JID = "jid";

    private final SharedPreferences mSharedPreferences;

    public SharedHelper(Context pContext) {
        mSharedPreferences = pContext.getSharedPreferences(NAME, Context.MODE_PRIVATE);
    }

    public void setLogin(String pLogin){
        mSharedPreferences.edit().putString(LOGIN, pLogin).apply();
    }

    public String getLogin(){
        return mSharedPreferences.getString(LOGIN, "");
    }

    public void setJid(String pJid){
        mSharedPreferences.edit().putString(JID, pJid).apply();
    }

    public String getJid(){
        return mSharedPreferences.getString(JID, "");
    }

}
