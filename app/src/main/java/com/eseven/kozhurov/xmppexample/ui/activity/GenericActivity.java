package com.eseven.kozhurov.xmppexample.ui.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.Toast;

import com.eseven.kozhurov.xmppexample.R;
import com.eseven.kozhurov.xmppexample.core.SharedHelper;
import com.eseven.kozhurov.xmppexample.core.XApplication;
import com.eseven.kozhurov.xmppexample.net.XmppManager;

public abstract class GenericActivity extends ActionBarActivity {

    protected XmppManager mXmppManager;
    protected SharedHelper mSharedHelper;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        XApplication xApplication = (XApplication) getApplication();
        mXmppManager = xApplication.getXmppManager();
        mSharedHelper = xApplication.getSharedHelper();
    }

    protected void showShortToast(int resId){
        showShortToast(getString(resId));
    }

    protected void showShortToast(String text){
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    protected void showProgressDialog(){
        if(mProgressDialog == null){
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.show();
        }
    }

    protected void hideProgressDialog (){
        if(mProgressDialog  != null){
            mProgressDialog.dismiss();
        }
    }

    protected void enableBackButton(){

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
