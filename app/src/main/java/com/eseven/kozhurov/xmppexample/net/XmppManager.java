package com.eseven.kozhurov.xmppexample.net;

import android.util.Log;

import com.eseven.kozhurov.xmppexample.core.MainCallBack;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.StanzaListener;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.filter.AbstractListFilter;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.Executor;

public final class XmppManager {

    private static final String XMPP_SERVER = "jabber.ru";

    private final Executor mExecutor;

    private XMPPTCPConnection mConnection;

    public XmppManager(Executor pExecutor) {
        mExecutor = pExecutor;
    }

    public void connectAndLoginToServer(final MainCallBack pMainCallBack, final String pLogin, final String pPassword) {

        mExecutor.execute(new Runnable() {
            @Override
            public void run() {

                mConnection = new XMPPTCPConnection(buildXmmpConnectionConfig(pLogin, pPassword));

                try {
                    mConnection.connect();
                    mConnection.login();
                    mConnection.sendStanza(new Presence(Presence.Type.available));

                    mConnection.addPacketInterceptor(new StanzaListener() {
                        @Override
                        public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
                            Log.e("XM", "packet = " + packet.toString());
                        }
                    }, null);
                    pMainCallBack.onSuccess();

                } catch (SmackException e) {
                    e.printStackTrace();
                    pMainCallBack.onError(e.getMessage());

                } catch (IOException e) {
                    e.printStackTrace();
                    pMainCallBack.onError(e.getMessage());

                } catch (XMPPException e) {
                    e.printStackTrace();
                    pMainCallBack.onError(e.getMessage());
                }
            }
        });
    }

    private XMPPTCPConnectionConfiguration buildXmmpConnectionConfig(String pLogin, String pPassword) {
        return XMPPTCPConnectionConfiguration.builder()
                .setHost(XMPP_SERVER)
                .setServiceName(XMPP_SERVER)
                .setDebuggerEnabled(true)
                .setUsernameAndPassword(pLogin, pPassword)
                .setCompressionEnabled(true)
                .setSendPresence(true)
                .setSecurityMode(ConnectionConfiguration.SecurityMode.disabled).build();
    }

    public void disconnect() {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    mConnection.disconnect(new Presence(Presence.Type.unavailable));
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Collection<RosterEntry> getRosterEntries() {
        if (mConnection == null || !mConnection.isConnected()) {
            Log.w("XM", "connection = "+mConnection + " , connected = "+mConnection.isConnected());
            return new HashSet<>();
        }

        return Roster.getInstanceFor(mConnection).getEntries();
    }

    public void setChatsListener(ChatManagerListener pChatManagerListener){
        ChatManager manager = ChatManager.getInstanceFor(mConnection);
        manager.addChatListener(pChatManagerListener);
    }

    public Chat connectToTheChat(String pThreadId, ChatMessageListener pMessageListener) {
        Chat chat = ChatManager.getInstanceFor(mConnection).getThreadChat(pThreadId);
        chat.addMessageListener(pMessageListener);
        return chat;
    }

    public Chat createChat(String pUserJid, ChatMessageListener pMessageListener) {
        return ChatManager.getInstanceFor(mConnection).createChat(pUserJid,pMessageListener);
    }

    public void sendMessageToChat(Chat pChat, String pMessage) {
        try {
            pChat.sendMessage(pMessage);
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
    }

    public void disconnectFromTheChat(Chat pChat, ChatMessageListener pMessageListener) {
        pChat.removeMessageListener(pMessageListener);
    }

}
