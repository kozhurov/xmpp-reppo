package com.eseven.kozhurov.xmppexample;

import android.app.Application;
import android.test.ApplicationTestCase;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class XApplicationTest extends ApplicationTestCase<Application> {
    public XApplicationTest() {
        super(Application.class);
    }
}